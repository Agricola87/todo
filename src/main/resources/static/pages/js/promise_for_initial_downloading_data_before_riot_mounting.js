var todoListResponse;
var message = { "name": "init_list" , "data" : {"token" : tokenString}};
var stringMessage = JSON.stringify(message);

function post() {
  return new Promise(function(succeed, fail) {
   $.ajaxSetup({
          beforeSend: function(xhr){
              xhr.setRequestHeader('X-CSRF-TOKEN', tokenString);
          }
        });
    $.ajax({
        url : "/command",
        type : "post",
        async: true,
        contentType: "application/json; charset=utf-8",
        data: stringMessage,
        dataType: "json",
        success : function(responseText) {
            todoListResponse = responseText;
            succeed("good");
            console.log(responseText);
        },
        error: function() {
            alert("Wrong response. Reload page");
        }
    });
});
}
post().then(function(text) {
    riot.mount('todo', todoListResponse)
}, function(error) {
  console.log(error);
});
