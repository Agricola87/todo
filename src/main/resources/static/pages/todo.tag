<todo>
    <div style = "color:red;" if = {(this.errorMode == true)}>
        {this.errorMessage}
    </div>

    <h3>{ opts.title }</h3>
    <!--  <h4>{ opts.token }</h4> -->

    <div>
        <button onclick={showSimpleTasks} disabled={this.taskType=="simple"}>Simple tasks</button>
        <button onclick={showCalendar} disabled={this.taskType=="calendar"}>Calendar</button>
        <button onclick={showProjects} disabled={this.taskType=="projects"}>Projects</button>
    </div>

    <div if={(this.taskType=="simple")}>
        <ul>
            <li each={ items.filter(whatShow) }>
                <button type="button" onclick={deleteItem2}>del</button>
                <button type="button" onclick={changeText}>edit</button>
                <label class={ completed: done }><input type="checkbox" checked={ done } onclick={ parent.toggle }>{title}</label>
            </li>
        </ul>
        <form onsubmit={ add }>
            <input ref="input" onkeyup={ edit } id="myInput">
            <button disabled={ !text }>Add #{ items.filter(whatShow).length + 1 }</button>
            <button type="button" disabled={ items.filter(onlyDone).length == 0 } onclick={ removeAllDone2 }>delete all done #{ items.filter(onlyDone).length }</button>
        </form>
        <hr>
        <button type="button" disabled={ this.showMode == 'all' } onclick={showAll }>Show all</button>
        <button type="button" disabled={ this.showMode == 'onlyDone' } onclick={ showOnlyDone }>Show done</button>
        <button type="button"  disabled={ this.showMode == 'onlyUndone' } onclick={ showOnlyUndone }>Show undone</button>
        <br><br>
        <hr>
        Test Commands
        <br><br>
        <button type="button" onclick={ reloadData }> Reload </button>
        <button style  = "background-color: red;" type="button" onclick={ sendCommandWithWrongName }>Send Command With Wrong Name </button>
        <br><br>
        <button style  = "background-color: red;" type="button" onclick={ sendRequestWithoutCommandName }>Send Request Without Command Name </button>
        <br><br>
        <button style  = "background-color: red;" type="button" onclick={ sendRequestWithoutData }>Try to add task without data in request </button>
        <br><br>
        <button style  = "background-color: red;" type="button" onclick={ doWrongSql }>Do wrong SQL </button>
    </div>

    <div if={(this.taskType=="calendar")}>Future calendar</div>
    <div if={(this.taskType=="projects")}>Future projects</div>

    <script>
    this.items = opts.items
    this.itemsToShow = this.items
    this.showMode = opts.showMode
    this.taskType = opts.taskType;
    this.token = opts.token;
    //ERRORS
    clearErrors(this);
    showSimpleTasks(){this.taskType = "simple";}
    showCalendar(){this.taskType = "calendar";}
    showProjects(){this.taskType = "projects";}
    edit(e) {this.text = e.target.value; }

    add(e) {
        clearErrors(this);
        var currThis = this;
        var message = { "name": "add_item",
          "data" : {"text" : this.text, "token" : tokenString}
     };
    var functionToDoAfterRequest = function(currentThis, respMsg){
        if(respMsg.response == "ok"){
            currentThis.items.push({ title: currentThis.text, id: respMsg.id })
            currentThis.text = currentThis.refs.input.value = '';
            //console.log(currentThis);
        }
        else if(respMsg.error = true)
        {
            currentThis.errorMode = true;
            currentThis.errorMessage += respMsg.errorMessage;
        }

     }
        doSynchrRequest(message, functionToDoAfterRequest, currThis);
        e.preventDefault()
    }
    removeAllDone2(e) {
        clearErrors(this);
        var ids = new Array();
        var preparedItems = this.items.filter(function(item) {
            if(item.done){
                ids.push(item.id);
            }
            return !item.done
        });
        var toChangeObject = {thisRef: 0, preparedItems: 0};
        toChangeObject.thisRef = this;
        toChangeObject.preparedItems = preparedItems;
        var message = { "name": "remove_all_done",
            "data" : {"ids" : ids, "token" : tokenString}
        };
        var functionToDoAfterRequest = function(toChangeObject, respMsg){
            if(respMsg.response == "ok"){
            toChangeObject.thisRef.items = toChangeObject.preparedItems;
            }
            else if(respMsg.error = true)
            {
                toChangeObject.thisRef.errorMode = true;
                toChangeObject.thisRef.errorMessage += respMsg.errorMessage;
            }
        }
        doSynchrRequest(message, functionToDoAfterRequest, toChangeObject);
    }
    whatShow(item) {
        if(this.showMode == 'all')
        {
            return !item.hidden
        }
        if(this.showMode == 'onlyDone')
        {
            return (!item.hidden)&&(item.done)
        }
        if( this.showMode == 'onlyUndone')
        {
            return (!item.hidden)&&(!item.done)
        }
    }

    onlyDone(item) {return item.done;}

    toggle(e) {
        clearErrors(this);
        //Что нужно менять в случае успешного ответа
        var item = e.item
        //готовим сообщение
        var message = { "name": "change_checkbox",
            "data" : {"id" : item.id, "token" : tokenString}
        };
        var toChangeObject = {thisRef: 0, preparedItems: 0};
        toChangeObject.thisRef = this;
        toChangeObject.curItem = item;
        //функция для выполнения после запроса
        var functionToDoAfterRequest = function(toChangeObject, respMsg){
            if(respMsg.response == "ok"){
                toChangeObject.curItem.done = !toChangeObject.curItem.done
            } else if(respMsg.error = true)
            {
                toChangeObject.thisRef.errorMode = true;
                toChangeObject.thisRef.errorMessage += respMsg.errorMessage;
            }
        }
        doSynchrRequest(message, functionToDoAfterRequest, toChangeObject);
        return true
    }
    deleteItem2(e) {
        clearErrors(this);
        var thisItem = e.item
        var preparedItems = this.items.filter(function(cur_item) {
            return cur_item.id != thisItem.id
        });
        var toChangeObject = {thisRef: 0, preparedItems: 0};
        toChangeObject.thisRef = this;
        toChangeObject.preparedItems = preparedItems;
        //передаем изменения на сервер
        var message = { "name": "delete_item",
            "data" : {"id" : thisItem.id, "token" : tokenString}
        };
        var functionToDoAfterRequest = function(toChangeObject, respMsg){
            if(respMsg.response == "ok"){
                toChangeObject.thisRef.items = toChangeObject.preparedItems;
            }
            else if(respMsg.error = true)
            {
                toChangeObject.thisRef.errorMode = true;
                toChangeObject.thisRef.errorMessage += respMsg.errorMessage;
            }
        }
        doSynchrRequest(message, functionToDoAfterRequest, toChangeObject);
        return true
    }

    showAll(e) {
    	  this.showMode = 'all'
    	  //e.preventDefault()
    	}

    showOnlyDone(e) {
    	  this.showMode = 'onlyDone';
    	 //e.preventDefault()
    }

    showOnlyUndone(e) {
    	  this.showMode = 'onlyUndone'
    	  //e.preventDefault()
    }

    sendCommandWithWrongName(e){
        var message = { "name": "test_command",
                "data" : {"token" : tokenString}
                };

        doTestRequest(message, testFunctionToDoAfterRequest, this);
    }

    sendRequestWithoutCommandName(e){
        var message = { "noname": "_",
                        "data" : {"token" : tokenString}
                        };

                doTestRequest(message, testFunctionToDoAfterRequest, this);
    }

    sendRequestWithoutData(e){
        var message =   { "name": "add_item",
                        "data" : {"notext" : "some text", "token" : tokenString}
        };

        doTestRequest(message, testFunctionToDoAfterRequest, this);
    }
    doWrongSql(e){
                      var message = { "name": "wrong_sql_command",
                              "data" : {"token" : tokenString}
                              };

                      doTestRequest(message, testFunctionToDoAfterRequest, this);
                  }
    changeText(e){
        clearErrors(this);
        var thisItem = e.item
        var title = thisItem.title;
        var idToDelete = thisItem.id;
        this.items = this.items.filter(function(cur_item) {
                return cur_item.id != thisItem.id
              });
        //удалить с сервера
        var message = { "name": "delete_item",
        "data" : {"id" : idToDelete, "token" : tokenString}
        };
        doAsyncRequest(message);

        document.getElementById("myInput").focus();
        this.refs.input.value = title;
    }
    reloadData(e){
        clearErrors(this);
        var message = { "name": "reload_data",
          "data" : {"token" : tokenString}
        };
        currThis = this;
      var stringMessage = JSON.stringify(message);
      $.ajaxSetup({
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-TOKEN', tokenString);
                }
              });
         $.ajax({
             url : "/command",
             type : "post",
             async: false,
             contentType: "application/json; charset=utf-8",
             data: stringMessage,
             dataType: "json",
             success : function(msg) {
             opts.title = msg.title;
                 currThis.items = msg.items;
                 console.log(msg);
             },
             error: function() {
                 alert("Wrong response. Reload page");
             }
         });
      e.preventDefault()
    }

    function doSynchrRequest(JSONMessage, functionToDoAfterRequest, smthToChange){
        var tokenString1 = JSONMessage.data.token;
            $.ajaxSetup({
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-TOKEN', tokenString1);
                }
            });
        var stringMessage = JSON.stringify(JSONMessage);
         $.ajax({
             url : "/command",
             type : "post",
             async: false,
             contentType: "application/json; charset=utf-8",
             data: stringMessage,
             dataType: "json",
             success : function(msg) {
//             console.log(msg);
                 functionToDoAfterRequest(smthToChange, msg);
             },
             error: function() {
                alert("Wrong response. Reload page");
             }
         });
    }
    function doAsyncRequest(JSONMessage){
        var tokenString = JSONMessage.data.token;
        var stringMessage = JSON.stringify(JSONMessage);
        $.ajaxSetup({
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-TOKEN', tokenString);
            }
        });
         $.ajax({
             url : "/command",
             type : "post",
             async: true,
             contentType: "application/json; charset=utf-8",
             data: stringMessage,
             dataType: "json",
             success : function(msg) {
             },
             error: function() {
                alert("Wrong response. Reload page");
             }
         });
    }
    function doTestRequest(JSONMessage, functionToDoAfterRequest, thisRef){
            var tokenString1 = JSONMessage.data.token;
                $.ajaxSetup({
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-TOKEN', tokenString1);
                    }
                });
            var stringMessage = JSON.stringify(JSONMessage);
             $.ajax({
                 url : "/command",
                 type : "post",
                 async: false,
                 contentType: "application/json; charset=utf-8",
                 data: stringMessage,
                 dataType: "json",
                 success : function(msg) {
                     functionToDoAfterRequest(thisRef, msg);
                 },
                 error: function() {
                    alert("Wrong response. Reload page");
                 }
             });
        }
        function testFunctionToDoAfterRequest (thisRef, respMsg){
            if(respMsg.error = true)
            {
                thisRef.errorMode = true;
                thisRef.errorMessage += respMsg.errorMessage;
            }
        }
    function clearErrors(thisReference){
        thisReference.errorMode = false;
        thisReference.errorMessage = "Something is gone wrong. You can send feedback about this error : ";
    }
    returnOneItem(e){
        var message = { "name": "return_one_item"
                };
        var stringMessage = JSON.stringify(message);
        $.ajaxSetup({
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-TOKEN', tokenString);
                        }
                      });
                 $.ajax({
                     url : "/command",
                     type : "post",
                     async: false,
                     contentType: "application/json; charset=utf-8",
                     data: stringMessage,
                     dataType: "json",
                     success : function(msg) {
                           console.log(msg);
                     },
                     error: function() {
                         alert("Wrong response. Reload page");
                     }
                 });
    }
  </script>

</todo>
