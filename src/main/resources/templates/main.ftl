
<#import "parts/login.ftl" as l>

<!doctype html>
<html>
<head>
    <title>Riot todo</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <link rel="stylesheet" href="/pages/todo.css">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script>html5.addElements('todo')</script>
    <![endif]-->
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="https://ff.kis.v2.scr.kaspersky-labs.com/C0A35687-268D-4147-9633-F393D28E4EA8/main.js" charset="UTF-8"></script><link rel="stylesheet" crossorigin="anonymous" href="https://ff.kis.v2.scr.kaspersky-labs.com/8AE4E82D393F-3369-7414-D862-78653A0C/abn/main.css"/></head>
<body>
<div>
    <@l.logout />
</div>

<!--${_csrf.token}-->


<todo></todo>
<script src="/pages/todo.tag" type="riot/tag"></script>
<script src="https://rawgit.com/riot/riot/master/riot%2Bcompiler.min.js"></script>

<script>
        tokenString = "${_csrf.token}";
</script>
<script src="/pages/js/promise_for_initial_downloading_data_before_riot_mounting.js"></script>

</body>
</html>