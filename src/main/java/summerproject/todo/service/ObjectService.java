package summerproject.todo.service;

import summerproject.todo.dao.ObjectTypeDAO;
import summerproject.todo.mappers.Task;
import summerproject.todo.util.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ObjectService extends Util implements ObjectTypeDAO {
    Connection connection = getConnection();
    //SQLReader создается при каждом вызове команды, чтобы гарантировать
    // явное указание в конструкторе папки с SQL запросами для данной команды

    @Override
    public ArrayList<Task> returnAllTasksByAuthUserId(Long authUserId) {
        SQLReader sqlReader = new SQLReader("return_all/");
        String sql = sqlReader.read("returnAllTasks.sql");
        PreparedStatement preparedStatement = null;
        ArrayList<Task> tasks = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, authUserId);
            ResultSet resultSet = preparedStatement.executeQuery();
            tasks = new ArrayList<Task>();
            while (resultSet.next()) {
                Task task = new Task();
                task.setTaskId(resultSet.getLong("id"));
                task.setDone(resultSet.getBoolean("isDone"));
                String decodedText = MyEncoder.decrypt(resultSet.getString("text"), authUserId);
                task.setText(decodedText);
                tasks.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tasks;
    }

    @Override
    public Long addSimpleTask(String text, Long authUserId) {

        String encodedText = MyEncoder.encrypt(text, authUserId);
        PreparedStatement preparedStatement = null;
        Long id = -1L;
        SQLReader sqlReader = new SQLReader("add_simple_task/");
        String insertTaskObjectSQL = sqlReader.read("insertTaskObject.sql");
        String insertTaskTypeParameterSQL = sqlReader.read("insertTaskTypeParameter.sql");
        String insertIsDoneParameterSQL = sqlReader.read("insertIsDoneParameter.sql");
        String insertTextParameterSQL = sqlReader.read("insertTextParameter.sql");
        String insertUserParameterSQL = sqlReader.read("insertUserParameter.sql");
        String getObjectIdSQL = sqlReader.read("getObjectId.sql");
        try{
            preparedStatement = connection.prepareStatement(insertTaskObjectSQL);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(insertTaskTypeParameterSQL);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(insertIsDoneParameterSQL);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(insertTextParameterSQL);
                preparedStatement.setString(1, encodedText);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(insertUserParameterSQL);
                preparedStatement.setLong(1, authUserId);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(getObjectIdSQL);
            ResultSet resultSet2 = preparedStatement.executeQuery();
            resultSet2.next();
            id = resultSet2.getLong("id");
        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return id;
    }

    @Override
    public boolean removeTask(Long id, Long authUserId) {
        boolean result = true;
        PreparedStatement preparedStatement = null;
        SQLReader sqlReader = new SQLReader("remove_task/");
        String removeTaskParametersSQL = sqlReader.read("removeTaskParameters.sql");
        String removeTaskObjectSQL = sqlReader.read("removeTaskObject.sql");
        try {
            preparedStatement = connection.prepareStatement(removeTaskParametersSQL);
                preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(removeTaskObjectSQL);
                preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            result = false;
            e.printStackTrace();
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return  result;
    }

    @Override
    public boolean checkUncheckTask(Long id) {
        boolean result = true;
        PreparedStatement preparedStatement = null;
        SQLReader sqlReader = new SQLReader("toogle_task/");
        String getCheckUncheckValueSQL = sqlReader.read("getCheckUncheckValue.sql");
        String changeCheckboxSQL;
        try {
            preparedStatement = connection.prepareStatement(getCheckUncheckValueSQL);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            boolean isDone = resultSet.getBoolean("isDone");
            if(isDone){
                changeCheckboxSQL = sqlReader.read("setUndone.sql");
            }else {
                changeCheckboxSQL = sqlReader.read("setDone.sql");
            }
            preparedStatement = connection.prepareStatement(changeCheckboxSQL);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            result = false;
            e.printStackTrace();
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public void addUserByAuthId(Long authUserId) {
        PreparedStatement preparedStatement = null;
        SQLReader sqlReader = new SQLReader("add_user_by_auth_id/");
        String addUserSQL = sqlReader.read("addUser.sql");
        String bindUserAndAuthUserSQL = sqlReader.read("bindUserAndAuthUser.sql");
        try {
            preparedStatement = connection.prepareStatement(addUserSQL);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(bindUserAndAuthUserSQL);
            preparedStatement.setLong(1, authUserId);
            preparedStatement.executeUpdate();

        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean wrongSQL() {
        boolean result = true;
        PreparedStatement preparedStatement = null;

        String wrongSQL = "incorrect sql";

        try {
            preparedStatement = connection.prepareStatement(wrongSQL);
            preparedStatement.executeQuery();
        } catch (SQLException e){
            result = false;
            e.printStackTrace();
        }
        finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

}
