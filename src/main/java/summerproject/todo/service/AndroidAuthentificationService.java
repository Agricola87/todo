package summerproject.todo.service;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import summerproject.todo.util.Util;

public class AndroidAuthentificationService extends Util {
    Connection connection = getConnection();
    PasswordEncoder pe = new BCryptPasswordEncoder(8);

    //for android
    public boolean checkUserCredentials(String uName, String uPassword) {
        boolean isUserAccepted = false;
        PreparedStatement preparedStatement = null;
        String checkUserPasswordSQL = "SELECT usr.password as encoded_password FROM db_todo_5.usr where usr.username = ?";
        try {
            preparedStatement = connection.prepareStatement(checkUserPasswordSQL);
            preparedStatement.setString(1, uName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            String encodedPassword =
                    resultSet.getString("encoded_password");
            isUserAccepted = pe.matches(uPassword, encodedPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isUserAccepted;
    }

    public Long returnUserId(String uName) {
        Long userId = -1L;
        PreparedStatement preparedStatement = null;
        String returnUserIdSQL = "SELECT usr.id as user_id FROM db_todo_5.usr where usr.username = ?";
        try {
            preparedStatement = connection.prepareStatement(returnUserIdSQL);
            preparedStatement.setString(1, uName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            userId = resultSet.getLong("user_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userId;
    }

}