package summerproject.todo.service;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class MyEncoder {
    public static String encrypt(String s, Long id_){
        Long id = id_*123456L;
        String seed = id.toString();

        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(seed);
        String encrypted= encryptor.encrypt(s);
        return encrypted;
    }

    public static String decrypt(String encrypted, Long id_) {
        Long id = id_*123456L;
        String seed = id.toString();
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(seed);

        String decrypted = encryptor.decrypt(encrypted);
        return decrypted;
    }

}
