package summerproject.todo.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SQLReader {
    public SQLReader(String folderName) {
        this.folderName = folderName;
    }

    private String rootPath = "src/main/java/summerproject/todo/service/sql/";
    private String folderName = "";

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public static String readAllBytesJava7(String filePath){
        String content = "";
        try
        {
            content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return content;
    }

    public String read(String fileName){
        //Обработка ошибок
        return readAllBytesJava7(rootPath + folderName + fileName);
    }
    public String safeRead(String folderName, String fileName){
        return  readAllBytesJava7(rootPath + folderName + fileName);
    }
}
