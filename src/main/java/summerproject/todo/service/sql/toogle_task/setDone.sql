UPDATE parameters
    SET boolean_value = 1
    WHERE attribute_id  =   (
                                SELECT attribute_id FROM attributes
                                    WHERE name = 'Is_ready_sign'
                            )
    AND object_id = ?