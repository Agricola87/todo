SELECT
    object_id
        as id,
    (SELECT text_value from parameters WHERE parameters.object_id = id and parameters.attribute_id = (SELECT attribute_id FROM attributes WHERE attributes.name = 'Text'))
        as text,
    (SELECT boolean_value from parameters WHERE parameters.object_id = id and parameters.attribute_id = (SELECT attribute_id FROM attributes WHERE attributes.name = 'Is_ready_sign'))
        as isDone,
    (SELECT text_value from parameters WHERE parameters.object_id = id and parameters.attribute_id = (SELECT attribute_id FROM attributes WHERE attributes.name = 'Task_type'))
        as taskType
    FROM objects
    WHERE object_type_id = (SELECT object_type_id FROM object_types where name = 'task')
    AND object_id IN
        (SELECT object_id FROM `references` as r
            WHERE
                r.referenced_object_id
                =
                (
                SELECT object_id FROM parameters
                    WHERE int_value = ?
                    AND parameters.attribute_id = (SELECT attribute_id FROM attributes WHERE attributes.name = 'auth_user_id')
                )
        )