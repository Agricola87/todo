INSERT INTO `references` (`object_id`, `attribute_id`, `referenced_object_id`)
    VALUES
    (
        (SELECT MAX(object_id) from objects),
        '8',
        (
            SELECT object_id FROM parameters
            WHERE attribute_id = (SELECT attribute_id FROM attribute_types WHERE name = 'auth_user_id')
            AND int_value = ?
        )
    )