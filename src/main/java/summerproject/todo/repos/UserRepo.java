package summerproject.todo.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import summerproject.todo.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
