package summerproject.todo.command;

import java.util.HashMap;
import java.util.Map;


public class VoidCommand extends TodoCommand {
    public VoidCommand() {}

    @Override
    public void exec() {
        System.out.println("I'm void command. I just do nothing.");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return new HashMap<String, Object>();
    }
}
