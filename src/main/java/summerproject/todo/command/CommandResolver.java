package summerproject.todo.command;
import summerproject.todo.todo_exceptions.NoCommandNameInRequestException;
import summerproject.todo.todo_exceptions.NoSuchCommandException;

import java.util.Map;

public class CommandResolver {

    public  TodoCommand getCommand(
            Map<String, Object> commandMap,
            Long authUserId
    ){
        String commandName = "";
        try{
            if(commandMap.get("name") == null){
                throw new NoCommandNameInRequestException();
            }
        }catch (NoCommandNameInRequestException e){
            return e.getErrorCommand();
        }
        commandName = (String) commandMap.get("name");

        try {
            if (commandName.equals("init_list")) {
                return new InitListCommand(commandMap, authUserId);
            } else if (commandName.equals("change_checkbox")) {
                return new ChangeCheckboxCommand(commandMap);
            } else if (commandName.equals("add_item")) {
                return new AddItemCommand(commandMap, authUserId);
            } else if (commandName.equals("reload_data")) {
                return new ReloadDataCommand(commandMap, authUserId);
            } else if (commandName.equals("delete_item")) {
                return new DeleteItemCommand(commandMap, authUserId);
            } else if (commandName.equals("remove_all_done")) {
                return new RemoveAllDoneCommand(commandMap, authUserId);
            } else if(commandName.equals("wrong_sql_command")){
                return new DoWrongSQLCommand();
            } else if(commandName.equals("return_one_item")){
                return new ReturnOneItemCommand(authUserId);
            }

            throw new NoSuchCommandException(commandName);
        } catch (NoSuchCommandException e){
            return e.getErrorCommand();
        }
    }
}
