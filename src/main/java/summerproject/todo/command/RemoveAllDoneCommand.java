package summerproject.todo.command;

import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.MyOwnSQLException;
import summerproject.todo.todo_exceptions.NoDemandedDataInRequestException;

import java.util.List;
import java.util.Map;

public class RemoveAllDoneCommand extends TodoCommand {
    private Long authUserId;

    public RemoveAllDoneCommand(Map<String, Object> commandMap, Long authUserId) {
        this.commandMap = commandMap;
        this.authUserId = authUserId;
    }

    @Override
    public void exec() {
        Object commandData = commandMap.get("data");
        try{
            if(((Map<String, Object>)commandData).get("ids") == null){
                throw new NoDemandedDataInRequestException("remove all done items", "ids");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object idsObject = ((Map<String, Object>)commandData).get("ids");
        List<Object> ids = (List)idsObject;

        for (Object item:ids) {
            ObjectService objectService = new ObjectService();
            String idString = (String)item;
            Long longId = Long.parseLong(idString, 10);
            try {
                if (!objectService.removeTask(longId, authUserId)) {
                    throw new MyOwnSQLException("RemoveDoneSQL");
                }
            } catch(MyOwnSQLException e) {
                e.setError(requestMap);
                return;
            }
        }
        requestMap.put("response", "ok");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return requestMap;
    }
}
