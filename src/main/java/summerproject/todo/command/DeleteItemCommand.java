package summerproject.todo.command;

import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.BadDataInRequestException;
import summerproject.todo.todo_exceptions.MyOwnSQLException;
import summerproject.todo.todo_exceptions.NoDemandedDataInRequestException;

import java.util.Map;

public class DeleteItemCommand extends TodoCommand {
    private Long authUserId;


    public DeleteItemCommand(Map<String, Object> commandMap, Long authUserId) {
        this.commandMap = commandMap;
        this.authUserId = authUserId;
    }

    @Override
    public void exec() {
        try{
            if(commandMap.get("data") == null){
                throw new NoDemandedDataInRequestException("delete item", "data");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object commandData = commandMap.get("data");
        try{
            if(((Map<String, Object>)commandData).get("id") == null){
                throw new NoDemandedDataInRequestException("delete item", "id");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object id = ((Map<String, Object>)commandData).get("id");
        String stringId = (String)id;

        Long longId = Long.parseLong(stringId, 10);
        try{
            if(longId == null){
                throw new BadDataInRequestException("delete item", "id");
            }
        }catch (BadDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        ObjectService objectService = new ObjectService();
        try {
            if (!objectService.removeTask(longId, authUserId)) {
                throw new MyOwnSQLException("RemoveItemSQL");
            }
        } catch(MyOwnSQLException e) {
            e.setError(requestMap);
            return;
        }
        requestMap.put("response", "ok");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return requestMap;
    }
}
