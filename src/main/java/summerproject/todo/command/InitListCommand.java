package summerproject.todo.command;
import summerproject.todo.mappers.Task;
import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.MyOwnSQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InitListCommand extends  TodoCommand {
    Long authUserId;
    Map<String, Object> map;
    public InitListCommand(Map<String, Object> commandMap, Long authUserId) {
        this.map = new HashMap<>();
        this.commandMap = commandMap;
        this.authUserId = authUserId;
    }

    @Override
    public void exec() {
        map.put("title", "My todo list");
        map.put("showMode", "all");
        map.put("taskType", "simple");
        ObjectService objectService = new ObjectService();
        ArrayList<Task> tasks = new ArrayList<Task>();
        ArrayList<Map<String, String>> items = new ArrayList<>();
        tasks = objectService.returnAllTasksByAuthUserId(authUserId);
        try {
            if (tasks == null) {
                throw new MyOwnSQLException("InitListSQL");
            }
        } catch(MyOwnSQLException e) {
            e.setError(requestMap);
            return;
        }

        for (Task task : tasks) {
            HashMap<String, String> item = new HashMap<>();
            item.put("title", task.getText());
            item.put("id", Long.toString(task.getTaskId()));
            if(task.isDone())
            {
                item.put("done", "true");
            }

            items.add(item);
        }
        map.put("items", items);
    }


    @Override
    public Map<String, Object> returnRequestMap() {
        return map;
    }
}
