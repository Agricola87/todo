package summerproject.todo.command;
import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.MyOwnSQLException;

import java.util.Map;

public class DoWrongSQLCommand extends TodoCommand {
    @Override
    public void exec() {
        ObjectService objectService = new ObjectService();
        objectService.wrongSQL();
        try {
            if(!objectService.wrongSQL()){
                throw new MyOwnSQLException("DoWrongSQLCommand");
            }
        } catch (MyOwnSQLException e){
            e.setError(requestMap);
            return;
        }
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return requestMap;
    }
}