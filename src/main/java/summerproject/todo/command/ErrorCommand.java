package summerproject.todo.command;

import java.util.HashMap;
import java.util.Map;

public class ErrorCommand extends TodoCommand {
    String errorMessage;

    public ErrorCommand(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public void exec() {
        System.out.println("I'm error command. ");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return new HashMap<String, Object>(){{put("error", "true");put("errorMessage", errorMessage);}};
    }
}
