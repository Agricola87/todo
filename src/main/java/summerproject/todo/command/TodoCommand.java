package summerproject.todo.command;

import java.util.HashMap;
import java.util.Map;

abstract public class TodoCommand {
    Map<String, Object> commandMap;
    protected HashMap<String, Object> requestMap = new HashMap<String, Object>();
    abstract public void exec();
    abstract public Map<String, Object> returnRequestMap();
}
