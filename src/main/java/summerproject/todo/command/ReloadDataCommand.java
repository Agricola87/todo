package summerproject.todo.command;


import summerproject.todo.mappers.Task;
import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.MyOwnSQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReloadDataCommand extends TodoCommand {
    Long authUserId;

    public ReloadDataCommand(Map<String, Object> commandMap, Long authUserId) {
        this.authUserId = authUserId;
        this.commandMap = commandMap;
    }

    Map<String, Object> map = new HashMap<>();

    @Override
    public void exec() {
        map.put("title", "My todo list - Reloaded");
        map.put("showMode", "all");
        map.put("taskType", "simple");
        ObjectService objectService = new ObjectService();
        ArrayList<Task> tasks = new ArrayList<>();
        ArrayList<Map<String, String>> items = new ArrayList<>();
        tasks = objectService.returnAllTasksByAuthUserId(authUserId);
        try {
            if (tasks == null) {
                throw new MyOwnSQLException("ReloadDataItemSQL");
            }
        } catch(MyOwnSQLException e) {
            e.setError(requestMap);
            return;
        }
        for (Task task : tasks) {
            HashMap<String, String> item = new HashMap<>();
            item.put("title", task.getText());
            item.put("id", Long.toString(task.getTaskId()));
            if(task.isDone())
            {
                item.put("done", "true");
            }
            items.add(item);
        }
        map.put("items", items);
        map.put("response", "ok");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return map;
    }
}
