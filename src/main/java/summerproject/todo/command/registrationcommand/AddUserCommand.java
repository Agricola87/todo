package summerproject.todo.command.registrationcommand;

import summerproject.todo.service.ObjectService;

public class AddUserCommand {
    public static void addUserByAuthId(Long authUserId){
        ObjectService objectService = new ObjectService();
        objectService.addUserByAuthId(authUserId);
    }
}
