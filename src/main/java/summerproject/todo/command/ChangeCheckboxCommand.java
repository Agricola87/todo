package summerproject.todo.command;

import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.BadDataInRequestException;
import summerproject.todo.todo_exceptions.MyOwnSQLException;
import summerproject.todo.todo_exceptions.NoDemandedDataInRequestException;

import java.util.Map;

public class ChangeCheckboxCommand extends TodoCommand {
    public ChangeCheckboxCommand(Map<String, Object> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public void exec() {
        try{
            if(commandMap.get("data") == null){
                throw new NoDemandedDataInRequestException("togle item", "data");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object commandData = commandMap.get("data");
        try{
            if(((Map<String, Object>)commandData).get("id") == null){
                throw new NoDemandedDataInRequestException("togle item", "id");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object id = ((Map<String, Object>)commandData).get("id");
        String stringId = (String)id;
        Long longId = Long.parseLong(stringId, 10);
        try{
            if(longId == null){
                throw new BadDataInRequestException("togle item", "id");
            }
        }catch (BadDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        ObjectService objectService = new ObjectService();
        try {
            if (!objectService.checkUncheckTask(longId)) {
                throw new MyOwnSQLException("TogleItemSQL");
            }
        } catch(MyOwnSQLException e) {
            e.setError(requestMap);
            return;
        }
        requestMap.put("response", "ok");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return requestMap;
    }
}
