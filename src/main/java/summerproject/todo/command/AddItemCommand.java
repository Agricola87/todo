package summerproject.todo.command;

import summerproject.todo.service.ObjectService;
import summerproject.todo.todo_exceptions.MyOwnSQLException;
import summerproject.todo.todo_exceptions.NoDemandedDataInRequestException;

import java.util.Map;

public class AddItemCommand extends TodoCommand {
    private Long authUserId;
    private Long newId;
    public AddItemCommand(Map<String, Object> commandMap, Long authUserId) {
        this.commandMap = commandMap;
        this.authUserId = authUserId;
    }

    @Override
    public void exec() {
        try{
            if(commandMap.get("data") == null){
                throw new NoDemandedDataInRequestException("add item", "data");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object commandData = commandMap.get("data");
        try{
            if(((Map<String, Object>)commandData).get("text") == null){
                throw new NoDemandedDataInRequestException("add item", "text");
            }
        }catch (NoDemandedDataInRequestException e){
            e.setError(requestMap);
            return;
        }
        Object text = ((Map<String, Object>)commandData).get("text");
        String textString = (String) text;
        ObjectService objectService = new ObjectService();
        newId = objectService.addSimpleTask(textString, authUserId);
        try {
            if(newId == -1L) {
                throw new MyOwnSQLException("AddItemSQL");
            }
        } catch (MyOwnSQLException e) {
            e.setError(requestMap);
            return;
        }
        requestMap.put("id", Long.toString(newId));
        requestMap.put("response", "ok");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return requestMap;
    }
}
