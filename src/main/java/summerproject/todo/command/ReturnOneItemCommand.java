package summerproject.todo.command;

import summerproject.todo.mappers.Task;
import summerproject.todo.service.ObjectService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReturnOneItemCommand extends TodoCommand {
    Long authUserId;
    HashMap<String, Object> returnItem;


    public ReturnOneItemCommand(Long authUserId) {
        this.authUserId = authUserId;
    }

    @Override
    public void exec() {

        ObjectService objectService = new ObjectService();
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks = objectService.returnAllTasksByAuthUserId(authUserId);
        Task task = tasks.get(0);
        HashMap<String, Object> item = new HashMap<>();
        item.put("title", task.getText());
        item.put("id", Long.toString(task.getTaskId()));
        returnItem = item;
        System.out.println("Return 1 item");
    }

    @Override
    public Map<String, Object> returnRequestMap() {
        return returnItem;
    }
}
