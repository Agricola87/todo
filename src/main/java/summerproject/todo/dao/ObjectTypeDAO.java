package summerproject.todo.dao;

import summerproject.todo.mappers.Task;

import java.util.ArrayList;

public interface ObjectTypeDAO {

    ArrayList<Task> returnAllTasksByAuthUserId(Long authUserId);
    Long addSimpleTask(String text, Long authUserId);
    boolean removeTask(Long id, Long authUserId);
    boolean checkUncheckTask(Long id);
    void addUserByAuthId(Long authUserId);
    boolean wrongSQL();
}
