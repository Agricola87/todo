package summerproject.todo.controllers;

import net.sf.uadetector.ReadableDeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import summerproject.todo.domain.User;
import summerproject.todo.repos.UserRepo;
import summerproject.todo.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class GeneralController {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }


    @GetMapping("/main")
    public String main(@RequestHeader("User-Agent") String userAgent, HttpServletResponse response, CsrfToken token){
        String stringToken = token.getToken();
        response.addHeader("token", stringToken);
//        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
//        ReadableUserAgent agent = parser.parse(userAgent);
//        ReadableDeviceCategory device = agent.getDeviceCategory();
//        System.out.println("\nDevice: " + device.getName());
//        if(device.getName().equals("Personal computer")){
            return "main";
//        }
//        if(device.getName().equals("Smartphone")){
//            return "androidmain";
//        }
//        return "unknowndevice";

    }

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model){
        User userFromDb = userRepo.findByUsername(user.getUsername());
        if(userFromDb != null){
            model.put("message", "User exists");
            return "registration";
        }
        userService.addUser(user);

        return "redirect:/login";
    }

}