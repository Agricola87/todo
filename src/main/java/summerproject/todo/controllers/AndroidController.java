package summerproject.todo.controllers;

import org.springframework.web.bind.annotation.*;
import summerproject.todo.command.InitListCommand;
import summerproject.todo.command.ReturnOneItemCommand;
import summerproject.todo.command.TodoCommand;
import summerproject.todo.command.VoidCommand;
import summerproject.todo.service.AndroidAuthentificationService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "androidcommand")
public class AndroidController {

    Long id;

    @GetMapping
    public @ResponseBody
    Map<String, Object>
    resolveCommand(
            @RequestParam(value="command", defaultValue="init") String commandName
//            , @RequestHeader HttpHeaders headers

            , @RequestParam(value="user", defaultValue="noname") String userName
            , @RequestParam(value="pass", defaultValue="nopass") String password
    ) {
        System.out.println("Android wants enter");
        TodoCommand command;
        AndroidAuthentificationService aus = new AndroidAuthentificationService();
        if(!aus.checkUserCredentials(userName, password)){
            command = new VoidCommand();
            return command.returnRequestMap();
        }
        id = aus.returnUserId(userName);


        if(commandName.equals("init")){
            command = new InitListCommand(new HashMap<String, Object>(), id);
        } else if(commandName.equals("onetask")){
            command = new ReturnOneItemCommand(id);
        }

        else{
            command = new VoidCommand();
        }
        command.exec();
        return command.returnRequestMap();
    }

}
