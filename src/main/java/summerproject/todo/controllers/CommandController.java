package summerproject.todo.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.*;
import summerproject.todo.command.CommandResolver;
import summerproject.todo.command.TodoCommand;
import summerproject.todo.domain.User;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping(value = "command")
public class CommandController {

    CommandResolver commandResolver = new CommandResolver();

    @PostMapping
    public @ResponseBody Map<String, Object>
    resolveCommand(
            @AuthenticationPrincipal User user,
            @RequestBody Map<String, Object> commandMap,
            @RequestHeader HttpHeaders headers,
            HttpServletResponse response,
            CsrfToken token
    ) {
        TodoCommand command = commandResolver.getCommand(commandMap, user.getId());
        command.exec();
        System.out.println("HEADERS ");

        System.out.println(headers.toString());

        response.addHeader("token", token.getToken());

        return command.returnRequestMap();
    }
}
