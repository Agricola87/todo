package summerproject.todo.todo_exceptions;

public class MyOwnSQLException  extends CommandException {
    String wrongSql;

    @Override
    public String getMessage() {
        return "Wrong SQL: " + wrongSql;
    }

    public MyOwnSQLException(String wrongSql) {
        this.wrongSql = wrongSql;
    }

    @Override
    public void printStackTrace() {
        System.out.println("Wrong SQL: " + wrongSql);
    }

}