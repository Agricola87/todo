package summerproject.todo.todo_exceptions;

import java.util.HashMap;

public abstract class CommandException extends Exception {
    public void setError(HashMap<String, Object> requestMap) {
        requestMap.put("error", "true");
        requestMap.put("errorMessage", getMessage());
    }
}
