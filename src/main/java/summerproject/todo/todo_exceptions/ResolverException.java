package summerproject.todo.todo_exceptions;

import summerproject.todo.command.ErrorCommand;

public abstract class ResolverException extends Exception {
    public ErrorCommand getErrorCommand(){
        return new ErrorCommand(getMessage());
    }

}
