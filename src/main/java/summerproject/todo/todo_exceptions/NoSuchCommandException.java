package summerproject.todo.todo_exceptions;

public class NoSuchCommandException extends ResolverException {
    String gottenCommandName;

    @Override
    public String getMessage() {
        return "No Such Todo Command: " + gottenCommandName;
    }

    public NoSuchCommandException(String gottenCommandName) {
        this.gottenCommandName = gottenCommandName;
    }

    @Override
    public void printStackTrace() {
        System.out.println("No Such Todo Command: " + gottenCommandName);
    }
}
