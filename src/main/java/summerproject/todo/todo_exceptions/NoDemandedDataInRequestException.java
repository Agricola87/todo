package summerproject.todo.todo_exceptions;

public class NoDemandedDataInRequestException extends CommandException {
    private String commandName;
    String demandedData;

    public NoDemandedDataInRequestException(String commandName, String demandedData) {
        this.commandName = commandName; this.demandedData = demandedData;
    }

    @Override
    public String getMessage() {
        return "No " + demandedData +" data in " + commandName + " command";
    }

    @Override
    public void printStackTrace() {
        System.out.println(getMessage());
    }
}
