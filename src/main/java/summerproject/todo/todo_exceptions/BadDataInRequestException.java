package summerproject.todo.todo_exceptions;

public class BadDataInRequestException extends CommandException {
    private String commandName;
    String dataName;

    public BadDataInRequestException(String commandName, String demandedData) {
        this.commandName = commandName; this.dataName = demandedData;
    }

    @Override
    public String getMessage() {
        return "Bad " + dataName +" data in " + commandName + " command";
    }

    @Override
    public void printStackTrace() {
        System.out.println(getMessage());
    }
}
