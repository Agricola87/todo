package summerproject.todo.todo_exceptions;

//This exception is used only in CommandResolver
public class NoCommandNameInRequestException extends ResolverException {
    @Override
    public void printStackTrace() {
        System.out.println(getMessage());
    }

    @Override
    public String getMessage() {
        return "No command name in request";
    }
}
